<?php

/**
 * Implementation of hook_form_alter().
 *
 * On user registration and editing forms, use a regular expression
 * to pass labels on CiviCRM forms through the t() localization function.
 *
 * Labels are enclosed in <label> tags.
 *
 * Needed because CiviCRM forms are rendered as HTML rather than passing
 * through Drupal's Forms API.
 */
function civicrm_localize_form_alter(&$form, $form_state, $form_id) {
  switch ($form_id) {
    case 'user_register':
    case 'user_edit':
      _civicrm_localize_alter($form);
      break;
  }
}

/**
 * Helper function for iterating and processing forms.
 */
function _civicrm_localize_alter(&$element) {
  // Match label, description table cell, and select tags.
  $regex_1 = '/(<label.*?>)(.*?)(<\/label>)/s';
  $regex_2 = '/(<td class="description">)(.*?)(<\/td>)/s';
  $regex_3 = '/(<select.*?>)(.*?)(<\/select>)/s';
  foreach (element_children($element) as $key) {
    // CiviCRM elements have no '#type' set. Assume if there is no '#type' that this
    // is a field we need to alter.
    if (isset($element[$key]['#value']) && !isset($element[$key]['#type'])) {
      $element[$key]['#value'] = preg_replace_callback($regex_1, '_civicrm_localize_replace', $element[$key]['#value']);
      $element[$key]['#value'] = preg_replace_callback($regex_2, '_civicrm_localize_replace', $element[$key]['#value']);
      $element[$key]['#value'] = preg_replace_callback($regex_3, '_civicrm_localize_select_replace', $element[$key]['#value']);
    }
  }
}

/**
 * General purpose helper function for localization.
 */
function _civicrm_localize_replace($matches) {
  // If a field is required, its label will have a span.
  // Detect and process this so the label is available for localization
  // without the span element.
  $regex = '/(.*?)(<span class="marker".*?>)(.*?)(<\/span>)/s';
  if (preg_match($regex, $matches[2], $out)) {
    // Localize title attribute text.
    $out[2] = str_replace('This field is required.', t('This field is required.'), $out[2]);
    $matches[2] = t($out[1]) .' '. $out[2] . $out[3]. $out[4];
  }
  else {
    $matches[2] = t($matches[2]);
  }
  return $matches[1] . $matches[2] . $matches[3];
}

/**
 * Helper function for localization of selects.
 */
function _civicrm_localize_select_replace($matches) {
  $options = array();
  $out = array();
  // Match options within the select element.
  $regex = '/(<option.*?>)(.*?)(<\/option>)/s';
  preg_match_all($regex, $matches[2], $out, PREG_SET_ORDER);
  foreach($out as $match) {
    $options[t($match[2])] = $match[1] . t($match[2]) . $match[3];
  }
  // Sort the options alphabetically by their new localized strings.
  // The first is, e.g., '- select -'. Remove it temporarily.
  $first = array_shift($options);
  ksort($options);
  // Add the first item back to the sorted list.
  array_unshift($options, $first);
  return $matches[1] . implode("\n", $options) . $matches[3];
}